<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
<link href="/css/bootstrap.min.css" rel="stylesheet">
	<title>Todo list</title>
</head>
<body>
<?php if($mess_error): ?>
	<div class="alert alert-danger">
		<?php foreach($mess_error as $m_error): ?>
			<p><?=$m_error;?></p>
		<?php endforeach; ?>	
	</div>
<?php endif; ?>
<?php if($mess_success): ?>
<div class="alert alert-info">
	<?php foreach($mess_success as $m_success): ?>
		<p><?=$m_success;?></p>
	<?php endforeach; ?>
</div>
<?php endif; ?>

<div>
	<?php if($auth): ?>
		<div class="card">
  <div class="card-body">
    <h5 class="card-title">Авторизация</h5>
    <p class="card-text">Вы соединились как администратор</p>
    <a href="/auth/logout/" class="card-link">Выйти</a>
  </div>
</div>
	<?php else: ?>
<!-- Button trigger modal -->

<button type="button" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#Modal1">
  Авторизация
</button>
	<?php endif; ?>
</div>

 <div class="list-group">
		<?php
			include ($contentPage);
		?>
 </div>
<div>
	<button type="button" class="btn btn-success" data-bs-toggle="modal" data-bs-target="#Modal2">
  Добавить задачу
</button>
</div>


<!-- Modal 1-->
<div class="modal fade" id="Modal1" tabindex="-1" aria-labelledby="Modal1Label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Modal1Label">Авторизация администратора</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="/auth/login/" method="get">
    	<div class="mb-3">
    		<label for="log1" class=form-label>Login</label>
		  <input type="text" class="form-control" id="log1" name="login">
    	</div>
    	<div class="mb-3">
		  	<label for="log2" class=form-label>Password</label>
		  <input type="text" class="form-control" name="password" id="log1">
    	</div>
    	<div class="mb-3"><input type="submit"></div>
	</form>
      </div>
    </div>
  </div>
</div>

<!-- Modal 2-->
<div class="modal fade" id="Modal2" tabindex="-1" aria-labelledby="Modal2Label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Modal2Label">Добавление задачи</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
        <form action="/index/save/" method="post">
	<div class="mb-3">
		<label for="lb1" class="form-label">Ваше Имя</label>
		<input type="text" name="name" class="form-control" id="lb1">
	</div>
	<div class="mb-3">
		<label for="lb1" class="form-label">Ваш Email</label>
		<input type="email" name="email" class="form-control" id="lb1" placeholder="name@example.com">
	</div>
	<div class="mb-3">
	  <label for="lb2" class="form-label">Введите задачу</label>
	  <textarea class="form-control" type="text" name="todo" id="lb2" rows="3"></textarea>
	</div>
    	<div class="mb-3"><input type="submit"></div>
	</form>
      </div>
    </div>
  </div>
</div>

<!-- Modal 3-->
<div class="modal fade" id="Modal3" tabindex="-1" aria-labelledby="Modal3Label" aria-hidden="true">
  <div class="modal-dialog">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="Modal3Label">Удаление задачи</h5>
        <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
      </div>
      <div class="modal-body">
      	<div class="mb-3">
					<p>Вы действительно хотите удалить задачу?<?=$todo['id'];?></p>
				</div>
				<a href="/index/delete/" class="card-link danger">Удаление</a>
      </div>
    </div>
  </div>
</div>


<script src="/js/bootstrap.min.js"></script>
<script>
const myModal = document.getElementById('myModal')
const myInput = document.getElementById('myInput')

myModal.addEventListener('shown.bs.modal', () => {
  myInput.focus()
})
</script>
</body>
</html>