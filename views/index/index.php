<?php if($todos): ?>
		<div class="row">
			<?php $field=0; ?>
			<?php foreach($grid_fields as $grid_field): ?>
			<div class="col">
				<?php if($sorts[$field]===$current_sort): ?>
				<h4 class="text-danger"><?=$grid_field;?></h4>
				<?php else: ?>
				<h4><?=$grid_field;?></h4>
				<?php endif; ?>
				<h4>
				<?php if(!($sorts[$field]===$current_sort && $current_sort_type===$sort_types[0])): ?>
					<a href="/index/?page=<?=$current_page;?>&sort=<?=$sorts[$field];?>&stype=<?=$sort_types[0];?>" >&#9660</a>
				<?php endif; ?>
				<?php if(!($sorts[$field]===$current_sort && $current_sort_type===$sort_types[1])): ?>
					<a href="/index/?page=<?=$current_page;?>&sort=<?=$sorts[$field];?>&stype=<?=$sort_types[1];?>">&#9650</a>
				<?php endif; ?>
				 </h4>
			</div>
			<?php $field++; ?>
			<?php endforeach; ?>
		</div>
	<?php foreach($todos as $todo): ?>
	<div class="list-group-item">
		<div class="row">
			<div class="col">
				<h4><?=$todo['user_name'];?></h4>
			</div>
			<div class="col">
				<h5><?=$todo['user_email'];?></h5>
			</div>
			<div class="col">
				<p><?=$todo['todo_text'];?></p>
			</div>
			<div class="col">
				<?php if(!$todo['status']==1): ?>
				<p>В работе</p>
				<?php else: ?>
				<p>Сделана</p>
				<?php endif; ?>
				<?php if($auth): ?>
				<br/>
				<p> 
					<a href="/index/delete/?id=<?=$todo['id'];?>">&#128465</a> <a href="/index/made/?id=<?=$todo['id'];?>">&#9989</a></p>
				<?php endif; ?>
			</div>
		</div>
	</div>
	<?php endforeach; ?>
<?php else: ?>
	<h2>Дела не найдены</h2>
<?php endif; ?>
<?php if($pages): ?>
	<nav aria-label="Page navigation">
		<ul class="pagination">
			<?php foreach($pages as $page): ?>
			<li class="page-item">
				<?php if($page===$current_page): ?>
					<a class="page-link active"><?=$page;?></a>
				<?php else: ?>
					<a class="page-link" href="/index/?page=<?=$page;?>&sort=<?=$current_sort;?>&stype=<?=$current_sort_type;?>"><?=$page;?></a>
				<?php endif; ?>
			</li>
			<?php endforeach; ?>	
		</ul>
	</nav>
<?php endif; ?>