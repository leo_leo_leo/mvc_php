<?php

Class Model_Todo Extends Model_Base {
	
	public $records;
	public $id;
	public $user_name;
	public $user_email;
	public $todo_text;
	public $status;
	public $corrected;
	
	public function fieldsTable(){
		return array(
			'id' => 'Id',
			'user_name' => 'User name',
			'user_email' => 'User email',
			'todo_text' => 'Todo text',
			'status' => 'Status',
			'corrected' => 'Corrected',
		);
	}

	public function getMaxPage($per_page) {
		return (int) ceil($this->records/$per_page);
	}

	public function getCountRecords(){
		try {
			$db = $this->db;
			$this->records=$db->query("SELECT COUNT(*) as count FROM todo")->fetchColumn();
		}	catch(PDOException $e) {
			echo $e->getMessage();
			exit;
		}
		return $this->records;
	}

	
}