<?php

function go_back($route) {
header('HTTP/1.1 200 OK');
header('Location: http://'.$_SERVER['HTTP_HOST'].$route);
exit();
}

function e($string)
{
    return htmlspecialchars($string, ENT_QUOTES, 'UTF-8');
};