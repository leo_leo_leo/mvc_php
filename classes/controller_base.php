<?php
// абстрактый класс контроллера
Abstract Class Controller_Base {
	protected $auth;
	protected $template;
	protected $layouts; // шаблон
	
	public $vars = array();

	private $field_name;
    private $message;
    private $type_of_rule;
    private $param;

	// в конструкторе подключаем шаблоны и авторизацию
	function __construct() {
		// авторизация
		$this->auth =  new Auth_Base();
		// шаблоны
		$this->template = new Template($this->layouts, get_class($this));
		$this->validator = new Validate_Base($this->field_name, $this->message, $this->type_of_rule, $this->param);
	}

	abstract function index();
	
}
