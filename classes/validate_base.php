<?php

class Validate_Base
{
    private $field_name;
    private $message;
    private $type_of_rule;
    private $param;
    private static $rules = [];
    private static $fields = [];
    private static $errors = [];
 
 
    public function __construct($field_name, $message, $type_of_rule, $param)
    {
        $this->field_name = $field_name;
        $this->message = $message;
        $this->type_of_rule = $type_of_rule;
        $this->param = $param;
    }
 
    public static function addRule($field_name, $message, $type_of_rule, $param = null)
    {
        self::$rules[] = new Validate_Base($field_name, $message, $type_of_rule, $param);
    }
 
    public static function addEntries($fields) {
        foreach ($fields as $fieldname => $value) {
            self::$fields[$fieldname] = self::sanitize($value);
        }
    }
 
    public static function validate() {
        foreach (self::$rules as $rule) {
            self::testRule($rule);
        }
    }
 
    public static function sanitize($text)
    {
        $text = trim(strip_tags($text));
 
        if (get_magic_quotes_gpc()) {
            $text = stripslashes($text);
        }
        return $text;
    }
   
    public static function getErrors()
    {
        if (count(self::$errors)) {
            return self::$errors;
        }
        return false;
    }
 
    public static function longerThan($value, $min)
    {
        if (strlen($value) >= $min) {
            return true;
        }
        return false;
    }
 
    public static function shorterThan($value, $max)
    {
        if (strlen($value) <= $max) {
            return true;
        }
        return false;
    }
 
    public static function asEmail($value)
    {
        if (filter_var($value, FILTER_VALIDATE_EMAIL)) {
            return true;
        }
        return false;
    }
 
    public static function asPhoneNumber($value)
    {
        if (preg_match("/^\(?[0-9]{3}\)? *-? *[0-9]{3} *-? *[0-9]{4}$/", $value)) {
            return true;
        }
        return false;
    }
 
    private static function testRule($rule)
    {
 
        if (isset(self::$errors[$rule->field_name])) {
            return;
        }
 
        if (isset(self::$fields[$rule->field_name])) {
            $value = self::$fields[$rule->field_name];
        }
        else {
            $value = null;
        }
 
        switch ($rule->type_of_rule) {
            case 'required' :
                if (empty($value)) {
                    self::$errors[$rule->field_name] = $rule->message;
                    return;
                }
                break;
            case 'minlength' :
                if (!(self::longerThan($value, $rule->param))) {
                    self::$errors[$rule->field_name] = $rule->message;
                    return;
                }
                break;
            case 'maxlength' :
                if (!(self::shorterThan($value, $rule->param))) {
                    self::$errors[$rule->field_name] = $rule->message;
                    return;
                }
                break;
            case 'email' :
                if (!(self::asEmail($value))) {
                    self::$errors[$rule->field_name] = $rule->message;
                    return;
                }
                break;
            case 'phonenumber' :
                if (!(self::asPhoneNumber($value))) {
                    self::$errors[$rule->field_name] = $rule->message;
                    return;
                }
                break;
        }
    }
 
}