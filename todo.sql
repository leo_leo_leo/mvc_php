-- база todo

  CREATE TABLE IF NOT EXISTS `todo` (
  `id` int(10) NOT NULL AUTO_INCREMENT,
  `user_name` varchar(255) DEFAULT NULL,
  `user_email` varchar(255) DEFAULT NULL,
  `todo_text` text,
  `status` tinyint(1) DEFAULT NULL,
  `corrected` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=utf8;


INSERT INTO `todo` (`user_name`, `user_email`, `todo_text`) VALUES
    ('Piter Solo', 'piter@piter.com', 'Тупая задача'),
    ('Piter Solo', 'piter@piter.com', 'Интересная задача'),
    ('Piter Solo', 'piter@piter.com', 'Засадная задача'),
    ('Piter Solo', 'piter@piter.com', 'Упертая задача'),
    ('Piter Solo', 'piter@piter.com', 'Упертая задача 2'),
    ('Piter Solo', 'piter@piter.com', 'Упертая задача 3'),
    ('Piter Solo', 'piter@piter.com', 'Упертая задача 4');