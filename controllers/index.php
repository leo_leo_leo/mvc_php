<?php
// контролер
Class Controller_Index Extends Controller_Base {
	
	// шаблон
	protected $layouts = "first_layout";
	// protected $validator;

	// экшен
	function index() {
		$per_page = 3;
		$mess_success = null;
		$mess_error = null;
		$todo_sort_by = ['user_name','user_email','todo_text','status'];
		$grid_fields = ['User','Email','Text','Status'];
		$todo_sort_type = ['ASC','DESC'];
		$current_mess = (isset($_GET['mess'])) ? $_GET['mess'] : null;
		$current_page = (isset($_GET['page'])) ? (int)$_GET['page'] : 1;
		$current_sort = in_array(isset($_GET['sort']),$todo_sort_by) ? $_GET['sort'] : 'user_name';

		switch ($current_mess) {
			case 'ERR_VAL': //ошибка валидации
			$mess_error = $_SESSION['ERR_VALID'];
				break;
			case 'SUC_ADD':
			$mess_success = array('успешно добавлена задача!');
				break;
			case 'SUC_DEL':
			$mess_success = array('успешно удалена задача!');
				break;
			case 'SUC_STAT':
			$mess_success = array('успешно обновлен статус!');
				break;
			case 'ERR_LOG':
			$mess_error = array('ошибка авторизации!');
				break;
			case 'SUC_LOG':
			$mess_success = array('успешная авторизация!');
				break;
			case 'SUC_OUT':
			$mess_success = array('Вы вышли из системы!');
				break;
		}

		$current_sort_type = in_array(isset($_GET['stype']),$todo_sort_type) ? $_GET['stype'] : 'ASC';

		$sorting = $current_sort . ' ' . $current_sort_type;
		$skip = '';
		if($current_page>1)
		{
			$skip .= (string) ($current_page-1)*$per_page;
			$skip .= ', ';
		}
		$skip .= (string) $per_page;
		
		// var_dump($current_sort);
		$select = array(
				'ORDER' => "$sorting",
				'LIMIT' => "$skip"
			);
		$model = new Model_Todo($select); // создаем объект модели
		$todos = $model->getAllRows();
		$todos_count = $model->getCountRecords();
		$todos_pages = $model->getMaxPage($per_page);

		$pages = array();
		for($i=1;$i<=$todos_pages;$i++)
			$pages[] = $i;

		$this->template->vars('auth', $this->auth->isAuth());
		$this->template->vars('mess_error', $mess_error);
		$this->template->vars('mess_success', $mess_success);
		$this->template->vars('todos', $todos);
		$this->template->vars('pages', $pages);
		$this->template->vars('grid_fields', $grid_fields);
		$this->template->vars('current_page', $current_page);
		$this->template->vars('sorts', $todo_sort_by);
		$this->template->vars('current_sort', $current_sort);
		$this->template->vars('sort_types', $todo_sort_type);
		$this->template->vars('current_sort_type', $current_sort_type);
		$this->template->view('index');
	}
// новая запись
	function save() {
		$this->validator::addRule(
                'name',
                'Поле User обязательное',
                'required');
		$this->validator::addRule(
                'name',
                'Имя должно состоять не менее чем из 2-х символов',
                'minlength',
                3);
		$this->validator::addRule(
                'email',
                'Поле email обязательное',
                'required');
		$this->validator::addRule(
                'email',
                'Поле email с ошибкой',
                'email');
		$this->validator::addRule(
                'todo',
                'Поле задача обязательное',
                'required');
		$this->validator::addEntries($_POST);
		$this->validator::validate();
		$errors = $this->validator::getErrors();
		if($errors){
		$_SESSION['ERR_VALID']=$errors;
		go_back('/index/?mess=ERR_VAL');
		}
		$model = new Model_Todo();
// Добавляем запись
		$model->user_name = e($_POST['name']);
		$model->user_email = e($_POST['email']);
		$model->todo_text = e($_POST['todo']);
		$model->save();
		go_back('/index/?mess=SUC_ADD');
	}
// Удаляем запись
	function delete() {
		$id = (isset($_GET['id'])) ? (int)$_GET['id'] : null;
		if($this->auth->isAuth() && $id) {
		$model = new Model_Todo();
		$result = $model->delRowById($id);
		go_back('/index/?mess=SUC_DEL');
		} else {
		go_back('/index/?mess=ERR_LOG');	
		}
	}
// Задача сделана
	function made() {
		$id = (isset($_GET['id'])) ? (int)$_GET['id'] : null;
		if($this->auth->isAuth() && $id) {
		$model = new Model_Todo();
		$result = $model->updateFieldById('status',1,$id);
		go_back('/index/?mess=SUC_STAT');
		} else {
		go_back('/index/?mess=ERR_LOG');
		}
	}
}